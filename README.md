![TBZ Logo](./x_gitressourcen/tbz_logo.png) 

# DSG - Datenschutz

## Kurzbeschreibung des Moduls 

Das Modul "Datenschutz" konzentriert sich auf die Vermittlung essenzieller Datenschutzbestimmungen, die bei der Entwicklung und dem Betrieb von IT-Services und Anwendungen von zentraler Bedeutung sind. In diesem Modul erlernen die Studierenden, wie sie Daten basierend auf ihrem Schutzbedarf effektiv kategorisieren können, und verstehen den kritischen Unterschied zwischen Datenschutz und Datensicherheit. Sie erhalten einen umfassenden Überblick über die Datenschutzgesetzgebung in verschiedenen Rechtsräumen, einschliesslich der Schweiz und der EU, und lernen die wichtigsten juristischen Dokumente wie das DSG und die DSGVO kennen. Darüber hinaus werden die Teilnehmenden darauf vorbereitet, die Einhaltung von Datenschutzgesetzen in angewandten Szenarien zu überprüfen und die Konsequenzen von Datenschutzverletzungen zu bewerten. Der Schwerpunkt liegt auf dem Datenschutzrecht, um Informatiker für die Herausforderungen im Umgang mit personenbezogenen Daten in der digitalen Welt zu rüsten.

## Angaben zum Transfer der erworbenen Kompetenzen 

 * Kategorisierung von Daten nach Schutzbedarf
 * Unterscheidung Datenschutz vs. Datensicherheit
 * Überblick Datenschutzgesetze (DSG, DSGVO) in verschiedenen Rechtsräumen (Schweiz, EU)
 * Prüfung von IT-Services und Anwendungen auf Datenschutzkonformität
 * Bewertung der Konsequenzen von Datenschutzverletzungen
 * Grundkenntnisse im Urheberrecht und Lizenzmodellen

## Unterlagen zum Modul

### Organisatorisches

[Organisatorisches ](0_Organisatorisches)zur Autorenschaft dieser Dokumente zum Modul

## Handlungskompetenzen

[Handlungskompetenzen](1_Handlungskompetenzen) zu den Handlungszielen 

### Unterrichtsressourcen

[Unterrichtsressourcen](2_Unterrichtsressourcen) von Aufträgen und Inhalten zu den einzelnen Kompetenzen

### Umsetzung

[Umsetzung](3_Umsetzung) 

### Fragekatalog

[Fragekatalog ](4_Fragekatalog) Allgemein

### Handlungssituationen

[Handlungssituationen ](5_Handlungssituationen)mit möglichen Praxissituationen zu den einzelnen Handlungszielen

### Zertifizierungen 

Mögliche [Zertifizierungen](8_Zertifizierungen) für dieses Modul 

- - - 

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/ch/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/">Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Schweiz Lizenz</a>.

- - - 

# Umsetzung

 - Bereich: Datenschutz und Datensicherheit gewährleisten
 - Semester: 3

## Lektionen

* Präsenz: 10
* Virtuell: 0
* Selbststudium: 10

## Lernziele
 - Kennt die Methoden zur Kategorisierung von Daten basierend auf ihrem Schutzbedarf und kann diese anwenden.
 - Versteht den Unterschied zwischen Datenschutz und Datensicherheit und kann Beispiele für Maßnahmen in beiden Bereichen nennen.
 - Besitzt einen umfassenden Überblick über die Datenschutzgesetze wie das DSG und die DSGVO in verschiedenen Rechtsräumen wie der Schweiz und der EU.
 - Kann IT-Services und Anwendungen hinsichtlich ihrer Konformität mit den Datenschutzgesetzen überprüfen und bewerten.
 - Ist in der Lage, die Konsequenzen von Datenschutzverletzungen zu erkennen und zu bewerten.
 - Verfügt über Grundkenntnisse in Urheberrecht und verschiedenen Lizenzmodellen und kann diese bei der Auswahl von Software anwenden.
 - Versteht den Unterschied zwischen Datenschutz und Urheberrecht

## Voraussetzungen

keine

## Technik

Tools zum kategorisieren von Daten und Erstellen von Datenschutzbestimmungen

## Methoden

Vorträge, Fallbeispiele

## Schlüsselbegriffe

Datenschutz, DSG, DSGVO, Personenbezogene Daten, Schutz der Privatsphäre, Schutz vor Missbrauch ihrer persönlichen Daten

## Lehr- und Lernformen

Lehrervorträge, Lehrgespräche, Workshop, Gastreferenten, Einzel- und Gruppenarbeiten, Lernfragen, Präsentationen

## Lehrmittel

Repository




